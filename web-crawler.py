import pathlib  # manage the filesystem
import requests  # requests
from time import sleep  # for delays

import os

from urllib.robotparser import RobotFileParser  # to parse robots.txt
from urllib.parse import urlparse  # to parse urls
from dotenv import load_dotenv

# Create environment variables
load_dotenv()

# Attached email for sysadmins to contact if issues
email = os.getenv("CONTACT_EMAIL")

# URL = "https://garden.org/plants/group"
USER_AGENT = f"plant-web-crawler ({email})"
USERAGENT = "*"
#
# _rp = {}
#
#
# def ask_robots(URL: str, useragent: str) -> bool:
#     url_struct = urlparse(URL)
#     base = url_struct.netloc
#     # look up in the cache or update it
#     if base not in _rp:
#         _rp[base] = RobotFileParser()
#         _rp[base].set_url(url_struct.scheme + "://" + base + "/robots.txt")
#         _rp[base].read()
#     return _rp[base].can_fetch(useragent, "/plants/group/")
#
#
# allowed_fetch = ask_robots(URL, USERAGENT)
# print(allowed_fetch)

from protego import Protego
import requests
import re

r = requests.get("https://garden.org/robots.txt")
URL = "https://garden.org/plants/group"

rp = Protego.parse(r.text)

print(rp.can_fetch(URL, "*"))
