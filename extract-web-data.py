import requests
import urllib.request
import time
from bs4 import BeautifulSoup
import lxml.html as lh
import pandas as pd
from urllib.request import Request, urlopen


# garden.org

# Security feature blocks spider/bot user agents so have to set a knwon user agent as below
# Following https://stackoverflow.com/questions/16627227/problem-http-error-403-in-python-3-web-scraping
req = Request("https://garden.org/plants/view/242735/Abelias-Vesalea/", headers={'User-Agent': 'Mozilla/5.0'})

# Byte object returned by the server and the content in webpage is mostly utf-8
web_byte = urlopen(req).read()

# Decode byte object using utf-8
webpage = web_byte.decode('utf-8')

print(webpage)

# Limit the crawling rate
# https://www.scraping-bot.io/how-to-scrape-a-website-without-getting-blocked/

# Create a URL list and a blank visited URL list

# Pop a link to be visited

# Parse the URL present on the page and add them to the URLs to be visited if they match the rules I've set
# And dont match any of the Visited URLs

# Scrape page